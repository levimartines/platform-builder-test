package com.levimartines.platformbuildertest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.levimartines.platformbuildertest.dto.ClienteDTO;
import com.sun.istack.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CLIENTE")
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CLI_ID")
    @JsonIgnore
    private Long id;

    @Column(name = "CLI_NOME")
    private String nome;

    @Column(name = "CLI_DTNASC")
    @NotNull
    private LocalDate nascimento;

    @CPF
    @NotNull
    @Column(name = "CLI_CPF")
    private String cpf;

    public Cliente(ClienteDTO dto) {
        this.nome = dto.getNome();
        this.nascimento = dto.getDataNascimento();
        this.cpf = dto.getCpf();
    }
}
