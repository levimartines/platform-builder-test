package com.levimartines.platformbuildertest.service;

import com.levimartines.platformbuildertest.dto.ClienteDTO;
import com.levimartines.platformbuildertest.exceptions.DataIntegrityException;
import com.levimartines.platformbuildertest.exceptions.ObjectNotFoundException;
import com.levimartines.platformbuildertest.model.Cliente;
import com.levimartines.platformbuildertest.repository.ClienteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ClienteService {

    private final ClienteRepository clienteRepository;


    public Cliente findById(Long id) {
        return clienteRepository.findById(id).orElseThrow(() ->
            new ObjectNotFoundException("Objeto não encontrado"));
    }

    public Page<ClienteDTO> findAll(String cpf, String nome, Integer page, String sortBy,
        String direction) {
        Page<Cliente> clientes;
        if (cpf != null && !cpf.isEmpty()) {
            clientes = clienteRepository.findDistinctByCpf(cpf, PageRequest.of(0, 20));
        } else {
            clientes = clienteRepository
                .findDistinctByNomeContaining(nome,
                    PageRequest.of(page, 10, Direction.fromString(direction), sortBy));
        }
        return clientes.map(ClienteDTO::new);
    }

    public Cliente save(ClienteDTO obj) {
        if (clienteRepository.findByCpf(obj.getCpf()) != null) {
            throw new DataIntegrityException("CPF ja cadastrado");
        }
        Cliente cliente = new Cliente(obj);
        clienteRepository.save(cliente);
        return cliente;
    }

    public void update(ClienteDTO obj, Long id) {
        Cliente newObj = findById(id);
        newObj.setNome(obj.getNome());
        newObj.setNascimento(obj.getDataNascimento());
        clienteRepository.save(newObj);
    }

    public void partialUpdate(String nome, Long id) {
        Cliente newObj = findById(id);
        newObj.setNome(nome);
        clienteRepository.save(newObj);
    }

    public void delete(Long id) {
        findById(id);
        clienteRepository.deleteById(id);
    }
}
