package com.levimartines.platformbuildertest.service;

import com.levimartines.platformbuildertest.model.Cliente;
import com.levimartines.platformbuildertest.repository.ClienteRepository;
import java.time.LocalDate;
import java.util.Arrays;
import org.springframework.stereotype.Service;

@Service
public class DbService {

    private final ClienteRepository repository;

    public DbService(ClienteRepository repository) {
        this.repository = repository;
    }

    public void instantiateDevDb() {
        LocalDate now = LocalDate.now();
        Cliente cli1 = new Cliente(null, "Test User", now.minusYears(33).minusDays(1), "00000014141");
        Cliente cli2 = new Cliente(null, "Cliente Teste 2", now.minusYears(36), "37565496073");
        Cliente cli3 = new Cliente(null, "Cliente Teste 3", now.minusYears(44), "62281864006");
        Cliente cli4 = new Cliente(null, "Cliente Teste 4", now.minusYears(51), "84455453024");
        Cliente cli5 = new Cliente(null, "Cliente Teste 5", now.minusYears(68), "54331313000");
        repository.saveAll(Arrays.asList(cli1, cli2, cli3, cli4, cli5));
    }
}
