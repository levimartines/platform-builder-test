package com.levimartines.platformbuildertest.repository;

import com.levimartines.platformbuildertest.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    @Transactional(readOnly=true)
    @Query("SELECT DISTINCT obj FROM Cliente obj WHERE obj.nome LIKE %:nome%")
    Page<Cliente> findDistinctByNomeContaining(@Param("nome") String nome, Pageable pageRequest);

    Cliente findByCpf(String cpf);

    Page<Cliente> findDistinctByCpf(@Param("cpf") String cpf, Pageable pageRequest);
}
