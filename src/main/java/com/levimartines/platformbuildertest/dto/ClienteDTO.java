package com.levimartines.platformbuildertest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.levimartines.platformbuildertest.model.Cliente;
import java.time.LocalDate;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class ClienteDTO {

    private Long id;

    @NotEmpty(message = "Preenchimento obrigatório")
    @Length(min = 5, max = 120, message = "O tamanho deve ser entre 5 e 120 caracteres")
    private String nome;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @NotNull
    private LocalDate dataNascimento;

    private Integer idade;

    @CPF(message = "CPF invalido")
    @NotEmpty(message = "Preenchimento obrigatório")
    private String cpf;

    public ClienteDTO(Cliente cliente) {
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.dataNascimento = cliente.getNascimento();
        this.idade = LocalDate.now().getYear() - cliente.getNascimento().getYear();
        this.cpf = cliente.getCpf();
    }
}
