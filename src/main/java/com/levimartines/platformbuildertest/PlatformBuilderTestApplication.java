package com.levimartines.platformbuildertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlatformBuilderTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlatformBuilderTestApplication.class, args);
    }

}
