package com.levimartines.platformbuildertest.controller;

import com.levimartines.platformbuildertest.dto.ClienteDTO;
import com.levimartines.platformbuildertest.service.ClienteService;
import com.levimartines.platformbuildertest.utils.URL;
import com.sun.istack.NotNull;
import java.net.URI;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<ClienteDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(new ClienteDTO(clienteService.findById(id)));
    }

    @GetMapping
    public ResponseEntity<Page<ClienteDTO>> findPage(
        @RequestParam(required = false, value = "cpf", defaultValue = "") String cpf,
        @RequestParam(required = false, value = "nome", defaultValue = "") String nome,
        @RequestParam(value = "page", defaultValue = "0") Integer page,
        @RequestParam(value = "sortBy", defaultValue = "nome") String sortBy,
        @RequestParam(value = "direction", defaultValue = "ASC") String direction) {
        return ResponseEntity
            .ok(clienteService.findAll(cpf, URL.decodeParam(nome), page, sortBy, direction));
    }

    @PostMapping
    public ResponseEntity<String> insert(@Valid @RequestBody ClienteDTO obj,
        HttpServletRequest request) {
        Long id = clienteService.save(obj).getId();
        URI uri = ServletUriComponentsBuilder.fromRequest(request)
            .path("/{id}").buildAndExpand(id).toUri();
        return ResponseEntity.created(uri).body("Created! ID: " + id.toString());
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        clienteService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity<Void> partialUpdate(@NotNull @NotEmpty @RequestBody String nome,
        @PathVariable @NotNull Long id) {
        clienteService.partialUpdate(nome, id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> update(@Valid @RequestBody ClienteDTO obj, @PathVariable Long id) {
        obj.setId(id);
        clienteService.update(obj, id);
        return ResponseEntity.noContent().build();
    }

}
