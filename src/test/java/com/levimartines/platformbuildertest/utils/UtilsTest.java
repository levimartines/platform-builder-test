package com.levimartines.platformbuildertest.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.levimartines.platformbuildertest.utils.URL;
import org.junit.jupiter.api.Test;

public class UtilsTest {

    @Test
    void urlDecoder() {
        String decodedString = URL.decodeParam("Levi%20Ma%3D");
        assertEquals(decodedString, "Levi Ma=");
    }

}
