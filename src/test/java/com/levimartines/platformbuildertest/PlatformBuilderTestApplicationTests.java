package com.levimartines.platformbuildertest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PlatformBuilderTestApplicationTests {

    @Test
    void contextLoads() {
        PlatformBuilderTestApplication.main(new String[] {});
    }

}
