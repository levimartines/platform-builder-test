package com.levimartines.platformbuildertest.cliente;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static com.levimartines.platformbuildertest.constants.ClienteConstants.NOME;
import static com.levimartines.platformbuildertest.constants.ClienteConstants.CPF;
import static com.levimartines.platformbuildertest.constants.ClienteConstants.NASCIMENTO;

import com.levimartines.platformbuildertest.dto.ClienteDTO;
import com.levimartines.platformbuildertest.model.Cliente;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class ClienteTests {

    @Test
    void dtoToCliente() {
        ClienteDTO dto = new ClienteDTO(1L, NOME, NASCIMENTO, null, CPF);
        assertEquals(dto.getId(), 1L);

        Cliente cliente = new Cliente(dto);
        assertEquals(cliente.getNome(), NOME);
        assertEquals(cliente.getCpf(), CPF);
        assertEquals(cliente.getNascimento(), NASCIMENTO);
    }

    @Test
    void clienteToDto() {
        Cliente cliente = new Cliente(1L, NOME, NASCIMENTO, CPF);
        ClienteDTO dto = new ClienteDTO(cliente);

        assertEquals(dto.getNome(), NOME);
        assertEquals(dto.getIdade(), 33);
        assertEquals(dto.getCpf(), CPF);
        assertEquals(dto.getDataNascimento(), NASCIMENTO);
    }

    @Test
    void cliente() {
        Cliente cliente = new Cliente();
        assertNotNull(cliente);
    }

    @Test
    void clienteDto() {
        ClienteDTO dto = new ClienteDTO();
        assertNotNull(dto);
    }

}
