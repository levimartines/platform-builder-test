package com.levimartines.platformbuildertest.cliente;

import static com.levimartines.platformbuildertest.constants.ClienteConstants.CPF;
import static com.levimartines.platformbuildertest.constants.ClienteConstants.NASCIMENTO;
import static com.levimartines.platformbuildertest.constants.ClienteConstants.NOME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.levimartines.platformbuildertest.dto.ClienteDTO;
import com.levimartines.platformbuildertest.exceptions.DataIntegrityException;
import com.levimartines.platformbuildertest.exceptions.ObjectNotFoundException;
import com.levimartines.platformbuildertest.model.Cliente;
import com.levimartines.platformbuildertest.service.ClienteService;
import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

@Slf4j
@SpringBootTest
public class ClienteServiceTests {

    @Autowired
    private ClienteService service;

    @Test
    void findById() {
        Cliente cliente = service.findById(1L);
        assertEquals(cliente.getNome(), NOME);
        assertEquals(cliente.getCpf(), CPF);
    }

    @Test
    void clienteNotFound() {
        try {
            service.findById(0L);
        } catch (RuntimeException e) {
            assertTrue(e instanceof ObjectNotFoundException);
            assertEquals("Objeto não encontrado", e.getMessage());
        }
    }

    @Test
    void findAll() {
        Page<ClienteDTO> clientes = service.findAll("", "", 0, "id", "ASC");
        assertTrue(clientes.getContent().size() > 0);
        clientes = service.findAll("00000014141", null, 0, "nome", "DESC");
        ClienteDTO cliente = clientes.getContent().get(0);
        assertEquals(cliente.getNome(), NOME);
        assertEquals(cliente.getCpf(), CPF);
    }

    @Test
    void save() {
        ClienteDTO dto = new ClienteDTO(null, NOME, NASCIMENTO, null, "27974059001");
        Cliente cliente = service.save(dto);
        assertNotNull(cliente.getId());
    }

    @Test
    void saveCpfJaCadastrado() {
        try {
            service.save(new ClienteDTO(null, NOME, NASCIMENTO, null, CPF));
        } catch (RuntimeException e) {
            assertTrue(e instanceof DataIntegrityException);
            assertEquals("CPF ja cadastrado", e.getMessage());
        }
    }

    @Test
    void update() {
        ClienteDTO dto = new ClienteDTO(null, "Updated Name", LocalDate.now(), null, null);
        service.update(dto, 2L);
        Cliente cliente = service.findById(2L);
        assertEquals(cliente.getNome(), "Updated Name");
    }

    @Test
    void partialUpdate() {
        service.partialUpdate("Patched Name", 2L);
        Cliente cliente = service.findById(2L);
        assertEquals(cliente.getNome(), "Patched Name");
    }

    @Test
    void delete() {
        service.delete(3L);
    }
}
