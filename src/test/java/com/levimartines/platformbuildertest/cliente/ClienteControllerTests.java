package com.levimartines.platformbuildertest.cliente;

import static com.levimartines.platformbuildertest.constants.ClienteConstants.CPF;
import static com.levimartines.platformbuildertest.constants.ClienteConstants.NOME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.levimartines.platformbuildertest.dto.ClienteDTO;
import com.levimartines.platformbuildertest.exceptions.ObjectNotFoundException;
import com.levimartines.platformbuildertest.model.Cliente;
import com.levimartines.platformbuildertest.service.ClienteService;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Slf4j
public class ClienteControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ClienteService service;

    @Test
    void idNotFound() {
        ResponseEntity<String> response = restTemplate
            .getForEntity("/clientes/0", String.class);
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    void getById() {
        ResponseEntity<ClienteDTO> response = restTemplate
            .getForEntity("/clientes/1", ClienteDTO.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        ClienteDTO cliente = response.getBody();
        assertNotNull(cliente);
        assertEquals(cliente.getNome(), NOME);
        assertEquals(cliente.getIdade(), 33);
        assertEquals(cliente.getCpf(), CPF);
    }

    @Test
    void getPage() {
        ResponseEntity<String> response = restTemplate.getForEntity("/clientes", String.class);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
    }

    @Test
    void saveInvalidCpf() {
        ClienteDTO dto = new ClienteDTO(null, "Uncle Bob",
            LocalDate.of(1952, 12, 5), null, "74754007001");
        ResponseEntity<String> response = restTemplate
            .postForEntity("/clientes", dto, String.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertNotNull(response.getBody());
    }

    @Test
    void saveDuplicatedCpf() {
        ClienteDTO dto = new ClienteDTO(null, "Uncle Bob",
            LocalDate.of(1952, 12, 5), null, "00000014141");
        ResponseEntity<String> response = restTemplate
            .postForEntity("/clientes", dto, String.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertNotNull(response.getBody());
    }

    @Test
    void saveInternalError() {
        Map<String, String> dto = new HashMap<>();
        dto.put("nome","Uncle Bob");
        dto.put("cpf", "74754007000");
        dto.put("dataNascimento", "dudsdus");
        ResponseEntity<String> response = restTemplate
            .postForEntity("/clientes", dto, String.class);
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    void save() {
        ClienteDTO dto = new ClienteDTO(null, "Uncle Bob",
            LocalDate.of(1952, 12, 5), null, "74754007000");
        ResponseEntity<String> response = restTemplate
            .postForEntity("/clientes", dto, String.class);
        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(response.getBody());
    }


    @Test
    void update() {
        LocalDate newUncleBobBirthDate = LocalDate.of(2020, 12, 5);
        ClienteDTO dto = new ClienteDTO(null, "Uncle Bob 2020", newUncleBobBirthDate, null, CPF);
        restTemplate.put("/clientes/3", dto);
        Cliente cliente = service.findById(3L);
        assertEquals(cliente.getNome(), "Uncle Bob 2020");
        assertEquals(cliente.getNascimento(), newUncleBobBirthDate);
    }

    @Test
    void delete() {
        ResponseEntity<String> response = restTemplate
            .getForEntity("/clientes/4", String.class);
        assertNotNull(response.getBody());

        restTemplate.delete("/clientes/4");
        response = restTemplate
            .getForEntity("/clientes/4", String.class);
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }

}
