package com.levimartines.platformbuildertest.constants;

import java.time.LocalDate;

public class ClienteConstants {

    public static final String NOME = "Test User";
    public static final LocalDate NASCIMENTO = LocalDate.now().minusYears(ClienteConstants.IDADE).minusDays(1);
    public static final int IDADE = 33;
    public static final String CPF = "00000014141";
}
