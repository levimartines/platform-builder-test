USE Teste;

CREATE TABLE cliente (
    cli_id integer not null auto_increment,
    cli_nome varchar(200) not null,
    cli_dtnasc datetime not null,
    cli_cpf varchar(16),
    primary key (CLI_ID)
);

SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;
SET collation_connection = utf8_general_ci;

INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Clark Kent',"2018-07-28","00000014141");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Levi Ferreira',"1995-05-21","59980407085");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Lorem Ipsum',"1991-12-11","46921120007");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Eric Cartman',"1965-03-13","40067152007");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Joao Gilberto da Silva',"1975-01-01","43585944094");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Shana Parry',"2000-07-18","29149013050");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Ellice Legge',"1915-05-01","18748663093");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Terence Diaz',"1944-12-31","86686633054");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Jobe Bell',"1932-03-23","31361652098");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Maheen Dalton',"1954-04-21","82568036060");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Ronan Molloy',"1988-10-01","47713720049");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Bogdan Cherry',"1990-11-13","53866016018");
INSERT INTO cliente (CLI_NOME, CLI_DTNASC, CLI_CPF) VALUES ('Marcos Mckenna',"1975-10-10","29318582005");

