# Platform Builder Test

Platform Builder Test for Java developers using Spring Boot.


## Getting Started


#### Executing program
* Open as a Maven Project
* From the root of project, build and run the docker image :

```
docker pull mysql:5.7
```

````
docker build -t dockerdb .
````
````
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=RootPassword -e MYSQL_DATABASE=Teste -e MYSQL_USER=MainUser -e MYSQL_PASSWORD=MainPassword dockerdb
````
* Run [PlatformBuilderTestApplication](src/main/java/com/levimartines/platformbuildertest/PlatformBuilderTestApplication.java)
##
#### Postman for API Appreciation
* try to use some of these requests! 
* https://documenter.getpostman.com/view/7428788/T1DjiyWw?version=latest

